import { combineReducers } from "redux";
import { productsReducer as products } from "./reducers/products";
import { modalReducer as modal } from "./reducers/modal";

export const rootReducer = combineReducers({ products, modal });
