import { magazineTypes } from "../types";

function getAllProducts(products) {
  return {
    type: magazineTypes.GET_ALL_PRODUCTS,
    payload: {
      products,
    },
  };
}

export function fetchProducts() {
  return async function (dispatch) {
    const products = await fetch("./product.json")
      .then((res) => res.json())
      dispatch(getAllProducts(products))
  };
}

export function addToCart(product) {
  return {
    type: magazineTypes.ADD_TO_CART,
    payload: {
      product,
    },
  };
}

export function removeFromCart(id) {
  return {
    type: magazineTypes.REMOVE_FROM_CART,
    payload: {
      id,
    },
  };
}
export function addToFavorite(product) {
  return {
    type: magazineTypes.ADD_TO_FAVORITE,
    payload: {
      product,
    },
  };
}

export function removeFromFavorite(id) {
  return {
    type: magazineTypes.REMOVE_FROM_FAVORITE,
    payload: {
      id,
    },
  };
}
