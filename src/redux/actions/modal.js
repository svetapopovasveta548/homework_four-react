import { magazineTypes } from "../types";

export function openModal(description, submitFunction) {
  return {
    type: magazineTypes.OPEN_MODAL,
    payload: {
      description,
      submitFunction,
    },
  };
}
export function closeModal() {
  return {
    type: magazineTypes.CLOSE_MODAL,
  };
}
