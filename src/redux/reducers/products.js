import { magazineTypes } from "../types";

const initialState = {
  products: [],
  productsCart: [],
  productsFavorite: [],
};

export function productsReducer(state = initialState, action) {
  switch (action.type) {
    case magazineTypes.ADD_TO_CART:
      return {
        ...state,
        productsCart: [...state.productsCart, action.payload.product],
      };
    case magazineTypes.REMOVE_FROM_CART:
      const filteredCartArray = state.productsCart.filter(
        (product) => product.id !== action.payload.id
      );
      return {
        ...state,
        productsCart: filteredCartArray,
      };
    case magazineTypes.ADD_TO_FAVORITE:
      return {
        ...state,
        productsFavorite: [...state.productsFavorite, action.payload.product],
      };
    case magazineTypes.REMOVE_FROM_CART:
      const filteredFavoriteArray = state.productsFavorite.filter(
        (product) => product.id !== action.payload.id
      );
      return {
        ...state,
        productsFavorite: filteredFavoriteArray,
      };
    case magazineTypes.GET_ALL_PRODUCTS:
      return {
        ...state,
        products: action.payload.products,
      };
    default:
      return state;
  }
}
