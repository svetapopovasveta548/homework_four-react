import { magazineTypes } from "../types";

const initialState = {
  isOpen: false,
  submitFunction: null,
  description: ""
};

export function modalReducer(state = initialState, action) {
  switch (action.type) {
    case magazineTypes.OPEN_MODAL: 
    return {
      ...state, 
      isOpen: true,
      submitFunction: action.payload.submitFunction,
      description: action.payload.description
    }
    case magazineTypes.CLOSE_MODAL: 
    return {
      ...state, 
      isOpen: false,
      submitFunction: null,
      description: ""
    }
    default:
      return state;
  }
}
