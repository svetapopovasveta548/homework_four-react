import React, { useState } from "react";
import ProductCards from "../ProductCards/ProductCards";
import "./ProductList.css"

function ProductList(props) {
  return (
    <div className="wrapper">
      {props.item && props.item.map((items) => (
        <ProductCards
page={props.page}
          key={items.id}
          data={items}
        />
      ))}
    </div>
  );
}

export default ProductList;
