import React from "react";
import "./Modal.css";
import modalWindowContent from "../Config/config";
import cross from "./cross-small-svgrepo-com (1).svg";
import { closeModal } from "../../redux/actions/modal";
import { useSelector, useDispatch } from "react-redux";

function Modal() {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);
  return modal.isOpen &&  (
    
    <div className="modal-background" onClick={() => dispatch(closeModal())}>
      <div className="modal-wrapper" onClick={(e) => e.stopPropagation()}>
        <img className="cross" onClick={() => dispatch(closeModal())} src={cross} alt="" />
        {/* {modalWindowContent.map((item) => (
          <p key={item.id}>{item.title}</p>
        ))} */}
        <p>{modal.description}</p>
        <button
          onClick={() => {
            modal.submitFunction();
            dispatch(closeModal());
          }}
        >
          Yes
        </button>
        <button onClick={() => dispatch(closeModal())}>No</button>
      </div>
    </div>
  );
}

export default Modal;
