import React, { useState, useEffect } from "react";
import "./ProductCards.css";
import star from "./star-svgrepo-com (1).svg";
import star2 from "./star-gold-orange-svgrepo-com.svg";
import { useSelector, useDispatch } from "react-redux";
import {
  addToCart,
  addToFavorite,
  removeFromCart,
  removeFromFavorite,
} from "../../redux/actions/products";
import { openModal } from "../../redux/actions/modal";

function ProductCards(props) {
  const dispatch = useDispatch();
  const [isTrue, setIsTrue] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const { title, price, url, id } = props.data;

  const productsInFavorite = useSelector(
    (state) => state.products.productsFavorite
  );
  const productsInCart = useSelector((state) => state.products.productsCart);

  useEffect(() => {
    const isInFavorite = productsInFavorite.find(
      (product) => product.id === id
    );
    isInFavorite && setIsFavorite(true);
    const isInCart = productsInCart.find((product) => product.id === id);
    isInCart && setIsTrue(true);
  }, [productsInFavorite, productsInCart, id]);

  const saveId = () => {
    props.c(id);
  };

  const toggle = () => {
    isFavorite ? deleteFavorite() : toFavorite();
  };

  const toFavorite = () => {
    dispatch(addToFavorite(props.data));
    setIsFavorite(true);
  };
  const deleteFavorite = () => {
    dispatch(removeFromFavorite(id));
    setIsFavorite(false);
  };

  const onClickSaveArrOfProduct = () => {
    //вызываю эту функцию
    // props.saveArrOfProduct(props.data);
    dispatch(addToCart(props.data));
    setIsTrue(true);
  };
  const deleteFromCart = () => {
    //вызываю эту функцию
    // props.saveArrOfProduct(props.data);
    dispatch(removeFromCart(id));
    setIsTrue(false);
  };

  return (
    <div>
      <div className="wrapper-product">
        <div onClick={toggle}>
          <img className="star" src={isFavorite ? star2 : star} alt="" />
        </div>

        <img className="img" src={url} alt="" />
        {title}
        <div>{price}</div>

        <button
          className="btn-add"
          onClick={() => {
            isTrue
              ? dispatch(openModal("Remove from cart", deleteFromCart))
              : dispatch(openModal("Add to Cart", onClickSaveArrOfProduct));
          }}
        >
          {(props.page == "cart" && "remove") || (isTrue ? "already in cart" : "add")}
        </button>
      </div>
    </div>
  );
}

export default ProductCards;
