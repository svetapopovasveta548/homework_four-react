import React, { useState, useEffect } from "react";
import Modal from "./components/Modal/Modal";
import { Routes, Route, NavLink } from "react-router-dom";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";
import { fetchProducts } from "./redux/actions/products";
import { useSelector, useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);
  const modal = useSelector((state) => state.modal);
  const [product, setProducts] = useState([]);
  const [productCart, setToCart] = useState([]);
  const [productFavorite, setToFavorite] = useState([]);

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  return (
    <>
      <div>{productFavorite.length}</div>
      <nav>
        <NavLink to="/">Home</NavLink>
        <NavLink to="/favorite">Favorite</NavLink>
        <NavLink to="/cart">Cart</NavLink>
      </nav>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/favorite" element={<Favorite />} />
        <Route path="/cart" element={<Cart />} />
      </Routes>
      <Modal />
    </>
  );
}

export default App;
