import ProductList from "../components/ProductList/ProductList"
import { useSelector } from "react-redux"


export default function Cart(props) {
  const productsCart = useSelector(state => state.products.productsCart)
  return  <ProductList page="cart"  item={productsCart} />

}

// onDeleteItem={props.onDeleteItem} 