import ProductList from "../components/ProductList/ProductList"
import { useSelector } from "react-redux"

export default function Favorite(props) {

  const productsFavorite = useSelector(state => state.products.productsFavorite)

  return  <ProductList page="favorite"  removeFromFavorite={props.removeFromFavorite}   item={productsFavorite} />

}