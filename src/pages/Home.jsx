import ProductList from "../components/ProductList/ProductList";
import { useSelector} from "react-redux";


export default function Home(props) {
  const products = useSelector(state => state.products.products)
  return (
    <ProductList
      item={products}
      page="home"
    />
  );
}
